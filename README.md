xlog 是一个web服务的日志分析工具，能够将访问日志进行ip、流量、状态码、URI，浏览器、爬虫等进行全面详细的分析。且支持以命令行模式进行分析，也支持以web的形式进行查看
3.1版本新增ip及地区的分析，ip库文件的路径在common/map.go进行修改，ip地址库在附件里下载
![输入图片说明](https://gitee.com/uploads/images/2017/1102/110328_590147c2_979382.png "命令行模式运行概况")

![输入图片说明](https://gitee.com/uploads/images/2017/1106/142957_db1b4601_979382.png "浏览器及爬虫走势")

![输入图片说明](https://gitee.com/uploads/images/2017/1031/175348_b0628bf2_979382.png "web模式运行概况")

![输入图片说明](https://gitee.com/uploads/images/2017/1031/175411_00f7927b_979382.png "在这里输入图片标题")

![输入图片说明](https://gitee.com/uploads/images/2017/1031/175501_2833a492_979382.png "在这里输入图片标题")

![输入图片说明](https://gitee.com/uploads/images/2017/1123/175100_9b91cf4e_979382.png "map.png")